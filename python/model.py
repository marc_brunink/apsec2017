from collections import defaultdict
import os
import logging
from random import randint

import numpy as np
from scipy.stats import norm
from scipy import stats
import matplotlib.pyplot as plt

from common.helper import load_object, save_object
from common.stopwatch import Stopwatch
from analysis.common import kmeans, gmm, kmeans_gmm



def load_model(item, percentage=100):
  model = None
  if os.path.isfile(item) and \
    (os.path.basename(item) == "model" or \
       os.path.splitext(item)[1] == ".model"):
    try:
      model = load_object(item)
      logging.info("Loaded model from cache")
    except:
      model = None
  if model is None:
    logging.warning("loading: {}".format(item))
    loader1 = LazyDataLoader(item)
    dirs = set()
    for file in loader1.files:
      dirs.add(os.path.dirname(file))
    machines = []
    for dir in dirs:
      logging.warning("Creating machine for {}".format(dir))
      loader = LazyDataLoader(dir)
      # FIXME remove the sampling percentage here
      machine = create_machine(loader, sampling_percentage=percentage)
      machine.name = dir
      machines.append(machine)
    logging.warning("machines: {}".format(machines))
    model = Model()
    model.add(machines)
    if os.path.exists("model"):
      logging.warning("Cannot store new model because file exists")
    else:
      save_object("model", model)
  return model



def read(file_name):
  """Yields tuples (block_no, boolean indicating whether it is a normal edge (True) or a return edge (False)"""
  with open(file_name) as f:
    for index, line in enumerate(f):
      try:
        fields = line.strip().split(" ")
        if len(fields) == 2:
          yield int(fields[0]), int(fields[1])
        elif len(fields) == 3:
          # this is a switch statement
          runtime_case_value = int(fields[1])
          cases = list(map(lambda x: int(x), fields[2].split(',')))
          try:
            case_index = cases.index(runtime_case_value) + 1
          except ValueError:
            # it is not in the case list -> it is default
            case_index = 0
          yield int(fields[0]), case_index
        else:
          raise ValueError("Failed to parse.")
      except Exception as e:
        raise ValueError("Could not parse {} line {}: {}".format(file_name, index, line)) from e



def create_machine(loader, sampling_percentage=100):
  model = MachineModel()
  model.files = []
  for file in loader.files:
    if sampling_percentage < 100:
      edges = sample_file(loader.edges(file), sampling_percentage)
    else:
      edges = loader.edges(file)
    model.add(get_traversal_count(edges))
    model.files.append(file)
  return model


def sample_file(data, percentage):
  '''
  :param data: data to sample
  :param percentage: percentage of data to sample
  :return: sampled data
  '''
  if percentage < 0 or percentage > 100:
    raise ValueError("Not a percentage {}. SHould be between 0 and 100".format(percentage))
  sample_every = 100 / percentage
  # 68% in one std dev
  # 95% in two std dev
  dev = 0.1 * sample_every # 68% are within 10% of mean
  # var = dev ** 2
  logging.warning("sample_every: {}".format(sample_every))
  rv = norm(loc=sample_every, scale=dev)
  try:
    total = len(data)
  except TypeError:
    # maybe a generator
    data = list(data)
    total = len(data)
  samples = []
  # samples.append(int(round(rv.rvs())))
  # we should definitely not use more than len(data) samples :D
  for distance in np.round(rv.rvs(size=len(data))):
    if len(samples) == 0:
      # samples.append(int(distance))
      # it might be good to start with a random offset
      # otherwise certain edges are oversampled and undersampled,
      # esp. without a random offest we probably gonna skip the
      # the first sample_every edges.
      # e.g. assume sample_every is 4 and the path is
      # 1->2->3->1->5->6->7->8
      # Then it is more likely we will sample 1->5 than 1->2
      # This might skew our analysis
      # not entirely sure whether this matters though.
      # Because in the end all samples should suffer from the same problem.
      # it just means that the mean of the samples is not equal to the mean of the population
      v = randint(0, sample_every)
      if v <= total:
        # short execution: the first sample might be already oob
        samples.append(v)
    else:
      v = samples[-1] + int(distance)
      if v > total:
        break
      samples.append(v)
  sampled = [data[index - 1] for index in samples]
  logging.warning("sampled : {} should be {}".format(len(sampled), len(data) / sample_every))
  return sampled



def get_traversal_count(edges):
  """Returns a dict[source][target] = count"""
  freq = {}
  for source, target in edges:
    try:
      freq[source][target] += 1
    except KeyError:
      if source not in freq:
        freq[source] = {}
      if target not in freq[source]:
        freq[source][target] = 0
      freq[source][target] += 1
  return freq


def reduce_dimensions(edges, matrix):
  redundant = []
  for index, column in enumerate(matrix.T):
    if len(set(column)) < 2:
      redundant.append(index)
      logging.warning("Marking edge as redundant because got single value only: {}".format(edges[index]))
    else:
      logging.warning("Marking edge {} as not as redundant because values : {}".format(index, set(column)))


  not_yet_redundant_edges = defaultdict(list)
  for index, (source, target) in enumerate(edges):
    logging.warning("checking {} {} {} for redundancy step 1".format(index, source, target))
    if index not in redundant:
      logging.warning("not redudant yet")
      not_yet_redundant_edges[source].append((index, target))
  for source, outgoing_edges in not_yet_redundant_edges.items():
    logging.warning("checking {} {} for redundancy".format(source, outgoing_edges))
    if len(outgoing_edges) == 1:
      # -> since we sample, this might still be either 0 (never sampled) or 1 (got sampled)
      # even iff we only have one target we should not do anything
      # Just skip. (This note is here in case you wonder)
      pass
    elif len(outgoing_edges) == 2:
      # keep only one
      # the other one does not carry any information since freq_1 = 1 - freq_2
      # Thus, it is redundant
      index1, target1 = outgoing_edges[0]
      index2, target2 = outgoing_edges[1]
      mean1 = np.mean(matrix.T[index1])
      mean2 = np.mean(matrix.T[index2])
      # We keep the one with the smaller mean. Why? Arbitrary decision
      # index = index1 if mean1 < mean2 else index2
      index = index1 if target1 < target2 else index2
      redundant.append(index)
    else:
      # assert 0, "{} has jumps to {}".format(source, outgoing_edges)
      logging.warning("{} has jumps to {}. This is caused by calls. Every call leads to a new edge"
                      .format(source, outgoing_edges))

  logging.warning("Redundant edges: {}".format(np.array(edges)[redundant]))
  indices_to_keep = [i not in redundant for i in range(matrix.shape[1])]
  new_matrix= matrix.T[indices_to_keep].T
  kept_edges = np.array(edges)[indices_to_keep]

  for column in new_matrix.T:
    assert len(set(column)) > 1, set(column)
  logging.warning("Reduced {} matrix to {}".format(matrix.shape, new_matrix.shape))
  assert new_matrix.shape[1] > 0
  return kept_edges, new_matrix


def get_reduced_edges_for_models(models):
  # calculate the common set of edges
  # reduce edges once
  # then always use these edges
  # benefit: the model does not have to be recreated because all edges are covered
  # otherwise a new model might introduce a new edge.
  # This will trigger the recalculation of the model.
  # Recalculation is costly
  all_machines = []
  for a_model in models:
    all_machines.extend(a_model.machines)
  edges, _ = reduce_dimensions(*get_matrix_for_models(all_machines))
  return edges



class LazyDataLoader(object):
  def __init__(self, directory):
    if not os.path.exists(directory):
      raise ValueError("Path {} does not exist.".format(directory))
    if not os.path.isdir(directory):
      raise ValueError("{} is not a directory".format(directory))
    self.files = self.find_files_(directory)
    self.directory = directory

  def find_files_(self, directory):
    from glob import glob
    globbed = glob(os.path.join(directory, '*'))
    if not globbed:
      globbed = [directory]
    logging.warning("globbed: {}".format(globbed))
    files = []
    for index, item in enumerate(globbed):
      if os.path.isdir(item):
        globbed_files = glob(os.path.join(item, '*'))
      elif os.path.isfile(item):
        globbed_files = [item]
      else:
        globbed_files = []
      for file in globbed_files:
        root, ext = os.path.splitext(file)
        if ext == ".coverage":
          files.append(file)
    logging.warning("files: {}".format(files))
    return files

  def __contains__(self, item):
    return item in self.files

  def edges(self, file):
    logging.warning("Lazy loader loads {}".format(file))
    return get_edges(read(file))


def get_edges(path):
  yield from path




def get_reduced_matrices_for_models(models, edges=None):
  if edges is None:
    # For reducing the dimensions use the normally normalized data
    # i.e. the observed edge traversal probabilities
    edges, _1 = reduce_dimensions(*get_matrix_for_models(models, normalized=True))
  # else:
  #   normalized_matrix = np.zeros(shape=(len(models), len(edges)))
  #   for index, model in enumerate(models):
  #     data = model.get_data_for_edges(edges, normalized=True)
  #     normalized_matrix[index] = data

  logging.warning("edges: {}".format(edges))
  _, non_normalized_matrix = get_matrix_for_models(models, normalized=False, edges=edges)

  # transform using sigmoid
  s = 7
  d = 0.05
  # non_normalized_matrix =  1/-s * np.log( ((1 + 2*d) / (non_normalized_matrix + d)) - 1) + 0.5
  # non_normalized_matrix =  np.log(non_normalized_matrix + 1)


  # normalise to total
  # In contrast, if you normalise on edge basis, i.e. you use the probability of a branch to go left or right,
  # then branches that are taken only once and go left during testing but right during in-field
  # execution, will have a severe impact.
  # Instead, we weight edges by normalising them to the total number of executed edges in an execution
  # we want to normalise to the total numbers of edges executed (the sum of the row)
  # _, normalized_matrix = get_matrix_for_models(models, normalized=True, edges=edges)
  normalized_matrix = (non_normalized_matrix.T / non_normalized_matrix.sum(axis=1)).T

  if not np.all(np.isfinite(normalized_matrix)):
    np.set_printoptions(threshold=np.nan)
    raise ValueError("result of normalization is not finite: {}".format(non_normalized_matrix))


  # non_normalized_matrix = np.zeros_like(normalized_matrix)
  # for index, model in enumerate(models):
  #   data = model.get_data_for_edges(edges, normalized=False)
  #   non_normalized_matrix[index] = data
  return edges, non_normalized_matrix, normalized_matrix

  # s = 7
  # d = 0.05
  # plot_matrix(all_models_matrix, prefix="{}-original".format(prefix))
  # transform using sigmoid
  # all_models_matrix =  1/-s * np.log( ((1 + 2*d) / (all_models_matrix + d)) - 1) + 0.5
  # plot_matrix(all_models_matrix, prefix="{}-sigmoid".format(prefix))
  # normalize dimensions
  # all_models_matrix -= np.min(all_models_matrix, 0)
  # all_models_matrix /= np.max(all_models_matrix, 0)
  # plot_matrix(all_models_matrix, prefix="{}-normalized".format(prefix))




def get_matrix_for_models(models, normalized=True, edges=None):
  if edges is None:
    edges = set()
    for model in models:
      edges.update(model.edges())
    edges = sorted(edges)
  matrix = np.zeros(shape=(len(models), len(edges)))
  for index, model in enumerate(models):
    data = model.get_data_for_edges(edges, normalized)
    matrix[index] = data
  return edges, matrix


def remove_same_rows(matrix):
  """remove redundant rows in matrix.

  :returns array of boolean indicating which rows got removed, new matrix"""
  if len(matrix) == 0:
    return np.array([False] * matrix.shape[0]), matrix

  total = 0
  all_same_rows = []
  processed_already = set()
  for index, row in enumerate(matrix):
    if index in processed_already:
      continue
    same_rows = np.all(matrix == row, axis=1)
    if sum(same_rows) > 1:
      indicies = map(lambda x: x[0], filter(lambda x: x[1], enumerate(same_rows)))
      processed_already.update(indicies)
      all_same_rows.append(same_rows)
      total += sum(same_rows) - 1

  # keep the first of a set of same rows
  # For this we find the first True and change
  # it to False
  for same in all_same_rows:
    for i, v in enumerate(same):
      if v:
        same[i] = False
        break
  all_same_rows = np.any(all_same_rows, axis=0)
  matrix = matrix[all_same_rows == False]
  return all_same_rows, matrix




def get_binom_bounds(k, n, p):
  def get_binom_bounds_steps(k, n, p, threshold, step):
    prev = 0 if step < 0 else 1
    for current_p in np.arange(p, 0 if step < 0 else 1, step):
      # logging.warning("binom: {} {} {}".format(k, n, current_p))
      cdf = stats.binom.cdf(k, n, current_p)
      # logging.warning("cdf: {}".format(cdf))
      # logging.warning("cdf: {}".format(cdf > threshold))
      if np.logical_and(cdf > threshold, cdf < 1 - threshold).all():
        # logging.warning("{} is possible".format(current_p))
        pass
      else:
        # logging.warning("{} is impossible".format(current_p))
        return prev
      prev = current_p
  return get_binom_bounds_steps(k, n, p, 0.001, -0.01), get_binom_bounds_steps(k, n, p, 0.001, 0.01)



class MachineModel(object):
  def __init__(self, name=None):
    # self.data = []
    self.name = name
    self.combined = {}

  def __str__(self):
    if self.name is not None:
      return "<MachineModel {}>".format(self.name)
    return super().__str__()


  def add(self, sample):
    # self.data.append(deepcopy(sample))
    # self.data.append(sample)
    for source in sample.keys():
      if source not in self.combined:
        self.combined[source] = {}
      for target in sample[source].keys():
        if target not in self.combined[source]:
          self.combined[source][target] = 0
        self.combined[source][target] += sample[source][target]


  def get_data_for_edges(self, edges, normalized=True):
    row = []
    # combine samples
    for source, target in edges:
      if source not in self.combined:
        row.append(0)
      elif target not in self.combined[source]:
        row.append(0)
      else:
        total = sum(self.combined[source].values())
        row.append(self.combined[source][target] / (total if normalized else 1))
        if (source == 707) and target == 717:
          logging.warning("707 to 717: {} {} {}".format( self.combined[source][target], total, self.combined[source][target] /total))
    return row

  def edges(self):
    for source, targets in self.combined.items():
      for target in targets.keys():
        edge = (source, target)
        yield edge

  def write_dot_file(self, file_name):
    with open(file_name, 'w') as f:
      print("strict digraph {", file=f)
      edges = list(self.edges())
      for fraction, value, (source, target) in zip(self.get_data_for_edges(edges),
                                         self.get_data_for_edges(edges, normalized=False),
                                         edges):
        print('{} -> {} [label="{:.1f}{}"];'.format(source, target, fraction * 100, value), file=f)
      print("}", file=f)

  def plot(self, file_name):
    dot_file = "{}.dot".format(file_name)
    self.write_dot_file(dot_file)
    from subprocess import check_call
    import shlex
    check_call(shlex.split("dot -Tpdf {} -o {}".format(dot_file, file_name)))





class Model(object):
  ''' A model is a collection of executions. Executions are called machines
  '''
  def __init__(self):
    self.machines = []
    self.invalidate_state_()

  # def __getstate__(self):
  #   return self.machines
  #
  # def __setstate__(self, state):
  #   self.machines = state
  #   self.invalidate_state_()


  def add(self, machines):
    self.machines.extend(machines)
    logging.warning("invalidating cause of adding")
    self.invalidate_state_()

  @property
  def edges(self):
    if not hasattr(self, 'edges_') or self.edges_ is None:
      self.calculate_matrix_()
    return self.edges_

  @edges.setter
  def edges(self, value):
    # Only if we have a state and the array are equal
    # take the shortcut
    # is self.edges_ is None we do not have state
    # so we would have to calculate it in order to be able to compare it.
    # the whole purpose of this return is to avoid calculating state if possible
    if self.edges_ is not None and np.array_equal(self.edges, value):
      return
    logging.warning("invalidating cause of edges: {}".format(value))
    for x,y in zip(self.edges, value):
      for x2, y2 in zip(x, y):
        if x2 != y2:
          logging.warning("different: {} != {}".format(x, y))
    self.invalidate_state_()
    self.edges_ = value

  @property
  def matrix_non_normalized(self):
    if self.matrix_non_normalized_ is None:
      self.calculate_matrix_()
    return self.matrix_non_normalized_

  @property
  def matrix_normalized(self):
    if self.matrix_normalized_ is None:
      self.calculate_matrix_()
    return self.matrix_normalized_

  def invalidate_state_(self):
    logging.warning("invalidating state")
    self.edges_, self.matrix_non_normalized_, self.matrix_normalized_ = None, None, None
    try:
      del self.gm_model_
    except AttributeError:
      pass
    try:
      del self.partitioning_
    except AttributeError:
      pass

  def calculate_matrix_(self):
    edges, self.matrix_non_normalized_, self.matrix_normalized_ = get_reduced_matrices_for_models(self.machines, edges=self.edges_)
    if self.edges_ is None:
      self.edges_ = edges

  @property
  def gm_model(self):
    if not hasattr(self, 'gm_model_') or self.gm_model_ is None:
      max_k = 10
      # max_k should be less or equal to no of samples (# of rows in matrix)
      if max_k > self.matrix_normalized.shape[0]:
        max_k = self.matrix_normalized.shape[0]
      with Stopwatch('Creating the model'):
        self.gm_model_, self.partitioning_ = gmm(self.matrix_normalized, min_k=1,
                                                 max_k=max_k, n_jobs=4)
      with Stopwatch("Finding the threshold"):
        # Do a Monte Carlo
        percentile = 1
        no_samples = 1000 * (1 / (percentile / 100))
        no_samples = min(no_samples, 100 * 1000)
        drawn, _ = self.gm_model.sample(no_samples)
        scores = self.gm_model.score_samples(drawn)
        self.score_threshold = np.percentile(scores, percentile)
        self.monte_carlo = scores
        self.monte_carlo_drawn = drawn
    return self.gm_model_

  @property
  def partitioning(self):
    return self.partitioning_


  def find_outliers(self, to_check, edges=None):
    all_machines = list(self.machines)
    all_machines.extend(to_check)
    logging.warning("Finding outliers")

    if edges is None:
      edges, _ = reduce_dimensions(*get_matrix_for_models(all_machines))
    self.edges = edges
    _, to_check_matrix_non_normalized, to_check_matrix_normalized = get_reduced_matrices_for_models(to_check, edges=edges)

    outliers = []
    for checked_model_index, (model, model_data_non_normalized, model_data_normalized) \
            in enumerate(zip(to_check, to_check_matrix_non_normalized, to_check_matrix_normalized)):

      this_is_an_outlier, est, machines_sharing_cluster, z_scores = self.is_outlier(
                      checked_model_index, model_data_normalized, model_data_non_normalized)
      if this_is_an_outlier:
        outliers.append((model, est, checked_model_index, self.matrix_normalized[checked_model_index], None, edges, z_scores))
    outliers.sort(key=lambda x: x[1])
    return outliers


  def is_outlier(self, checked_model_index, model_data_normalized, model_data_non_normalized):
    cluster_of_model = self.gm_model.predict(model_data_normalized)
    probabilities = self.gm_model.predict_proba(model_data_normalized)
    logging.warning("model weights: {}".format(self.gm_model.weights_))
    logging.warning("model weights: {}".format(self.gm_model.weights_))
    logging.warning("model esimate weights: {}".format(self.gm_model._estimate_log_weights()))
    logging.warning("model _estimate_log_prob: {}".format(self.gm_model._estimate_log_prob(self.matrix_normalized)))
    logging.warning("model {} has probabilities: {}".format(checked_model_index, probabilities))
    logging.warning("Checked for outlier model {} has score: {}"
                    .format(checked_model_index, self.gm_model.score(model_data_normalized)))
    logging.warning("model {} has scores: {}"
                    .format(checked_model_index, self.gm_model.score_samples(self.matrix_normalized)))
    logging.warning("centroids: {}".format(self.gm_model.means_))

    models_in_same = np.array(list(map(lambda x: x[0],
                                       filter(lambda x: x[1], enumerate(self.partitioning == cluster_of_model)))))
    # models_in_same += models_in_same >= checked_model_index
    logging.warning("models in same cluster {}".format(models_in_same))
    for e, index in enumerate(models_in_same):
      logging.warning("In same cluster: {} {}".format(e, self.machines[index]))

    # calculate z score per dimension
    models_same_cluster_matrix = self.matrix_normalized[self.partitioning == cluster_of_model]

    devs = np.std(models_same_cluster_matrix, 0)
    means = np.mean(models_same_cluster_matrix, 0)
    means_all = np.mean(self.matrix_normalized, 0)
    z_scores = (model_data_normalized - means) / devs


    import math
    if not hasattr(self, "indo"):
      self.indo = 0
    self.indo += 1

    pdfs = []
    kdes = []
    for index, column in enumerate(self.matrix_normalized.T):
      logging.warning("Calculating kde for {} edge {} col: {}".format(index, self.edges[index], column.shape))
      with Stopwatch("Calculating kde"):
        # kde = stats.gaussian_kde(self.monte_carlo_drawn.T[column_id])
        try:
          kde = stats.gaussian_kde(column)
        except np.linalg.linalg.LinAlgError:
          # might be singular matrix
          # All the values of these edges are probably the same
          # (This might happen if we enabled edges manually)
          # THIS IS A HACK.
          column[0] += 0.01
          kde = stats.gaussian_kde(column)

      # kde.set_bandwidth(0.01  * kde.factor)
      # kde(self.monte_carlo_drawn.T[column_id])
      logging.warning("model_data_norm {} ".format(model_data_normalized))
      logging.warning("model_data_norm {} ".format(model_data_normalized.shape))

      pdf = kde.pdf(model_data_normalized.T[index])
      pdfs.append(pdf[0])
      kdes.append(kde)



    for pdf_score, kde, edge, mean, p, k, column, (column_id, _), mean_all in sorted(zip(pdfs, kdes,
                                                                                  self.edges, means,
                                                                                  model_data_normalized,
                                                                                  model_data_non_normalized,
                                                                                  models_same_cluster_matrix.T,
                                                                                  enumerate(models_same_cluster_matrix.T), means_all),
                                                    key=lambda x: math.fabs(x[0]) if not math.isnan(x[0]) else -1,
                                                                              reverse=True):
      n = k/p
      with Stopwatch("get binom bounds"):
        bounds = get_binom_bounds(k, n, p)
      logging.warning("edge {} k: {} n: {} p: {} bounds: {} mean: {} mean_all: {} z-score: {}"
                      .format(edge, k, n, p, bounds, mean, mean_all, pdf_score))

      # Calculate one-dimensional KDE to decide whether to draw the graph


      logging.warning("Sampling kde. samples: {}".format(len(column)))
      with Stopwatch("sampling kde"):
        samples = kde.resample(1000)[0]

      thres = np.percentile(kde.pdf(samples), 1)
      logging.warning("thres: {}".format(thres))

      logging.warning("my value p: {}".format(p))
      logging.warning("my value p shape: {}".format(p.shape))
      logging.warning("my value: {}".format(kde.pdf(p)))
      samples = samples[0]

      larger = kde.pdf(p) >= thres
      if not larger:
        # plot the model distribution and the outlier data point
        plt.close()
        plt.hist(column, bins=50, normed=True)
        plt.hist(self.monte_carlo_drawn.T[column_id], bins=50, histtype="step", normed=True)
        plt.hist(samples, bins=50, histtype="step", normed=True)
        plt.axvline(p)
        plt.savefig("{}-{}-{}-{}-{}.pdf".format("l" if larger else "s", str(self.indo), str(edge[0]),
                                                str(edge[1]), str(kde.pdf(p)[0])))

      lower, upper = bounds
      if lower is None:
        lower = 0
      if upper is None:
        upper = 1
      if p < lower or p > upper:
        logging.warning("VIOLATED")

    logging.warning("self.score_threshold: {}".format(self.score_threshold))
    logging.warning("model_data_normalized shape: {}".format(model_data_normalized.shape))
    score = self.gm_model.score(model_data_normalized)
    fraction = sum(self.monte_carlo < score) / len(self.monte_carlo)
    logging.warning("fraction: {}".format(fraction))


    if score >= self.score_threshold:
      # This is not an outlier
      return False, 0, models_in_same, []
    else:
      # This is an outlier
      logging.warning("{} is an outlier".format(checked_model_index))
      return True, 1, models_in_same, z_scores
