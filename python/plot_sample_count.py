import logging
logging.getLogger().setLevel("INFO")

from scipy.spatial.distance import cdist, pdist

from model import *
import matplotlib.pyplot as plt

def main():
  import sys
#  assert len(sys.argv) == 3
  models = []
  for option in sys.argv[1:]:
    models.append(load_model(option))
  assert len(models) > 0

  matrices = [get_matrix_for_models(model.machines, normalized=False)[1] for model in models]
  # each row is a executions in a model
  no_data_points = [np.sum(matrix, axis=1) for matrix in matrices]
  for e in no_data_points:
    logging.warning("e {}".format(e))
    # e /= len(e)
    logging.warning("e {}".format(e))

  fig = plt.figure(figsize=(4, 3))
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.36, bottom=0.2, right=0.93, top=0.99, wspace=0, hspace=0)


  for data in no_data_points:
    plt.hist(data, histtype='step', bins=100000, label='la', normed=True, cumulative=True)


  logging.warning("means: {}".format([np.mean(data) for data in no_data_points]))
  logging.warning("medians: {}".format([np.median(data) for data in no_data_points]))
  logging.warning("standard deviation: {}".format([np.std(data) for data in no_data_points]))
  logging.warning("percentiles: {}".format([np.percentile(data, [1,5,25,50,75,95,99]) for data in no_data_points]))
  logging.warning("executed edges: {}".format([matrix.shape for matrix in matrices]))

  ax.set_xscale("log")
  # plt.xlim(0,100000)

  plt.legend()
  fig.savefig("sample_count.pdf")


if __name__ == '__main__':
  main()