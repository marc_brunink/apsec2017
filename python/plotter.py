from scipy.spatial.distance import cdist, pdist
import logging
logging.getLogger().setLevel("INFO")
from common.helper import load_object


distance_metric = 'euclidean'
import numpy as np
def calculate_distance(test_case_matrix):
  dimensions = test_case_matrix.shape[1]
  max_distance = pdist([[0] * dimensions, [1] * dimensions], distance_metric)
  samples = 1000
  sample = np.random.rand(samples, dimensions)
  cd = cdist(sample, test_case_matrix)
  return 1
  # return max_distance
  return np.mean(np.min(cd, axis=1))



import sys
import os.path
loaded = []
for option in sys.argv[1:]:
  logging.warning("argv: {}".format(option))
  
  shape, curve, test_case_matrix, execution_matrix = load_object(option)
  name, ext = os.path.splitext(os.path.basename(option))
  logging.warning("name: {}".format(name))
  logging.warning("shape: {}".format(shape))
  # if name in ['head', 'du', 'df']:
  #   logging.warning("skip")
  #   continue
  loaded.append((name, shape, curve, test_case_matrix, execution_matrix))


from matplotlib import pyplot as plt
# loaded = loaded[:int(len(loaded)/2)]
# loaded = loaded[int(len(loaded)/2):]

fig = plt.figure(figsize=(4, 3))
ax = fig.add_subplot(111)
plt.subplots_adjust(left=0.18, bottom=0.15, right=0.95, top=0.95, wspace=0, hspace=0)

for label, (samples, dimensions), data, test_case_matrix, execution_matrix in loaded:
  logging.warning("test_case_matrix: {}".format(test_case_matrix.shape))
  logging.warning("execution_matrix: {}".format(execution_matrix.shape))
  if test_case_matrix.shape[0] < 20:
    logging.warning("Skipping {} because it has only {} test case(s)"
                    .format(label, test_case_matrix.shape[0]))
    continue
  if execution_matrix.shape[0] < 20:
    logging.warning("Skipping {} because we only have {} sample(s)"
                    .format(label, execution_matrix.shape[0]))
    continue
  # mkdir low dimensionality and test cases do not achieve statement coverage
  # # "mkdir -p" should trigger it. They did not test missing option.
  to_skip = ['cat', 'cp', 'expr', 'head', 'mkdir', 'rm', 'tail', 'tr']
  # if we do not normalize, we can also skip these.
  to_skip.extend(['mv'])
  # to_skip.extend(['mv', 'du', 'cut'])
  to_skip = []
  if label in to_skip:
    logging.warning("Skipping {} because we manually excluded it."
                    .format(label))
    continue
  logging.warning("Plotting {}".format(label))
  distance = calculate_distance(test_case_matrix)
  data = [(x / distance, y) for x, y in data]

  label = label.split('-')[0]
  # if label not in ['cut', 'df', 'du', 'ls', 'sort', 'wc']:
  if label not in ['df', 'ls', 'sort', 'wc']:
    label=None
    color = 'grey'
  else:
    color = None

  ax.plot(list(map(lambda x: x[0], data)), list(map(lambda x: x[1], data)), label=label, color=color)

from matplotlib.ticker import FuncFormatter
ax.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y)))
plt.xlim(0, None)
plt.ylim(0, 1)
plt.xlabel('Distance Threshold')
plt.ylabel('Coverage')


plt.legend()
fig.savefig("distances.pdf")