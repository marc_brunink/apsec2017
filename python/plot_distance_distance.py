import logging
logging.getLogger().setLevel("INFO")

from scipy.spatial.distance import cdist, pdist

from model import *
import matplotlib.pyplot as plt



def main():
  import sys
#  assert len(sys.argv) == 3
  models = []
  try:
    percentage = float(sys.argv[1])
    remaining_opts = sys.argv[2:]
  except ValueError:
    percentage = 100
    remaining_opts = sys.argv[1:]
  logging.warning("percentage: {}".format(percentage))
  for option in remaining_opts:
    logging.warning("argv: {}".format(option))
    models.append(load_model(option, percentage))
  assert len(models) == 3

  edges = get_reduced_edges_for_models(models)

  # make sure the 2 models have the same order in names
  # comes in handy later
  models[1].machines.sort(key=lambda machine: machine.name)
  models[2].machines.sort(key=lambda machine: machine.name)
  models[1].invalidate_state_()
  models[2].invalidate_state_()
  names1 = [machine.name for machine in models[1].machines]
  names2 = [machine.name for machine in models[2].machines]
  assert names1 == names2

  matrices = [get_matrix_for_models(model.machines, normalized=True,
                                          edges=edges)[1] for model in models]
  # test cases are in index 0 by convention
  # remove same test cases (ls has a lot of them)
  # this speeds up calculation a bit (I guess. Never checked)
  _, matrices[0] = remove_same_rows(matrices[0])
  distances = cdist(matrices[1], matrices[0], metric='cityblock')
  closest1 = np.min(distances, axis=1)
  distances = cdist(matrices[2], matrices[0], metric='cityblock')
  closest2 = np.min(distances, axis=1)

  matrices2 = [get_matrix_for_models(model.machines, normalized=False,
                                          edges=edges)[1] for model in models]
  no_of_samples = np.sum(matrices2[2], axis=1)

  # Do a scatter plot
  # plt.clear()
  plt.close()
  fig = plt.figure(figsize=(4, 3))
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.16, bottom=0.2, right=0.93, top=0.99, wspace=0, hspace=0)
  from matplotlib.colors import LinearSegmentedColormap
  colors = [(0.85, 0.85, 0.85), (0, 0, 0)] 
  cmap = LinearSegmentedColormap.from_list('mymap', colors)
  sc = ax.scatter(closest1, closest2, marker='.', c=np.log(no_of_samples+1), cmap=cmap)

  from matplotlib.ticker import FuncFormatter
  clb = plt.colorbar(sc, format=FuncFormatter(lambda y, _: int(np.round(np.exp(y)))),
                     ticks=np.log(np.power(10, range(6))),
                     shrink=0.8)
  clb.ax.set_title('Samples', loc='left', fontdict={'fontsize' : 10})
  pos = clb.ax.get_position()
  pos2 = pos.get_points()
  diff = (pos2[1][1] - pos2[0][1]) * 0.1
  pos2[0][1] -= diff
  pos.set_points(pos2)
  clb.ax.set_position(pos)

  maximum = np.max((np.max(closest1), np.max(closest2)))
  maximum += 0.25
  plt.xlim(0, maximum)
  plt.ylim(0, maximum)
  plt.xlabel('Distance to nearest test run \n(no sampling)')
  plt.ylabel('Distance to nearest test run \n(sampling rate: 1%)')
  # plt.legend()
  fig.savefig("distance_distance.pdf")

if __name__ == '__main__':
  main()
