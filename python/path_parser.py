from analysis.common import kmeans, gmm, kmeans_gmm
import matplotlib.pyplot as plt
from common.helper import load_object, load_objects, save_object, save_objects

from common.stopwatch import Stopwatch
from scipy.cluster import hierarchy
from model import *


import logging
logging.getLogger().setLevel("INFO")

from scipy.spatial.distance import cdist, pdist





def get_vector(file_path):
  # file might be quite large
  # -> thus, do not read it into memory
  # instead read it whenever it is used
  # and use the generator directly
  traversals = get_traversal_count(get_edges(read(file_path)))

  outgoing = defaultdict(lambda: defaultdict(int))
  incoming = defaultdict(lambda: defaultdict(int))
  basic_blocks = set()
  for (bb1, bb2), count in traversals.items():
    outgoing[bb1][bb2] = count
    incoming[bb2][bb1] = count
    basic_blocks.add(bb1)
    basic_blocks.add(bb2)
  basic_blocks = sorted(basic_blocks)

  if len(outgoing[21]) == 1:
    print("{} has only 21->23: {}".format(file_path, outgoing[21]))

  invocations = {}
  vectors = {}
  for block in basic_blocks:
    total_outgoing = sum(outgoing[block].values())
    total_incoming = sum(incoming[block].values())
    # total_outgoing != total_incoming for start and termination basic_blocks
    invocations[block] = max(total_outgoing, total_incoming)
    if total_outgoing == 0:
      continue
    for target, v in outgoing[block].items():
      print("{} -> {} = {}, {}".format(block, target, v, v / total_outgoing))
      edge = "{} -> {}".format(block, target)
      vectors[edge] = (v, v / total_outgoing)
  
  # print invocations (should be in a different function)
  # total_invocations = sum(invocations.values())
  # for basic_block, invocation in invocations.items():
  #   print("invocations {}: {:.2f} {}".format(basic_block, invocation / total_invocations, invocation))
  
  return vectors


def get_matrix(vectors):
  from itertools import chain
  all_edges = sorted(set([e for e in chain(*map(lambda x: x.keys(), vectors.values()))]))

  matrix = np.zeros(shape=(len(vectors), len(all_edges)))
  all_files = sorted(vectors.keys())
  
  for index, file in enumerate(all_files):
    vector = vectors[file]
    row = [vector[k][0] if k in vector else 0 for k in all_edges]
    # row.extend([vector[k][0] if k in vector else 0 for k in all_edges])
    # row.extend([1 if k in vector else 0 for k in all_edges])
    matrix[index] = row

  return all_files, all_edges, matrix
      

def write_dot_file(vector, file_name):
  with open(file_name, 'w') as f:
    print("strict digraph {", file=f)
    for edge, (value, normalised) in vector.items():
      print('{} [label="{}|{:2f}"];'.format(edge, value, normalised), file=f)
    print("}", file=f)


def write_daikon_file(file, column_names, matrix):
  with open(file, 'w') as f:
    from itertools import chain
    s = ", ".join(chain(map(str, column_names), map(lambda x: str(x) + " count", column_names),
                        map(lambda x: str(x) + " flag", column_names)))
    print(s, file=f)
    print(matrix.shape)
    for row in matrix:
      s = ", ".join(map(str, row))
      print(s, file=f)

import subprocess
import os.path


def convert_daikon_csv(file):
  # Use this after uograde to Python 3.5
  # subprocess.run(['/home/marc/Daikon/daikon-5.4.4/scripts/convertcsv.pl', file], check=true)
  subprocess.check_call(['/home/marc/Daikon/daikon-5.4.4/scripts/convertcsv.pl', file])

  name, ext = os.path.splitext(file)
  file1 = name + ".dtrace"
  file2 = name + ".decls"
  assert os.path.isfile(file1), "No dtrace file"
  assert os.path.isfile(file2), "No decls file"
  return [file1, file2]


def run_daikon(files):
  args = ['java', 'daikon.Daikon']
  args.extend(files)
  output = subprocess.check_output(args)
  output = output.decode()
  output = output[output.index('POINT') + 5:output.index('Exiting')]
  return output


def run_daikon_for_all_nodes(column_names, matrix):
  node_edges = defaultdict(lambda: list())
  for index, column_name in enumerate(column_names):
    source, _, target = column_name.split()
    print(node_edges[source])
    print(index)
    # Need to check here whether is is present already because
    # we might have reflexive nodes.
    # we do not use a set because the order might be important(?)
    if index not in node_edges[source]:
      node_edges[source].append(index)
    if index not in node_edges[target]:
      node_edges[target].append(index)

  for node, edges in node_edges.items():
    print('Running daikon for {}'.format(node))
    rows = matrix.shape[0]
    print(rows)
    print(matrix.shape)
    # matrix = matrix[*edges]
    print('+++')
    print(edges)
    new_matrix = matrix[:, edges]
    # new_matrix = np.zeros(shape=(rows, len(edges)))
    # for index, edge in enumerate(edges):
    #   print(index)
    #   print(new_matrix.shape)
    #   print(matrix[edge])
    #   print(len(matrix[edge]))
    #   new_matrix[index] = matrix[,edge]
    print(new_matrix.shape)
    # print(new_matrix)
    print("Node: {}".format(node))
    print("edges: {}".format(edges))
    edge_names = [column_names[i] for i in edges]
    name = '{}.csv'.format(node)
    write_daikon_file(name, edge_names, new_matrix)
    files = convert_daikon_csv(name)
    output = run_daikon(files)
    print('Daikon output for {}'.format(node))
    print(output)


def sample_row(histogram, no_samples):
  """ input: an array of absolute edge counts (histogram)
      returns an array of same dimensions with no_samples samples
      drawn uniformly from all observed edges."""
  def sample(row, total):
    to_sample = randint(1, total)
    accumulated = 0
    for index, value in enumerate(row):
      accumulated += value
      if accumulated >= to_sample:
        return index
    assert 0

  copied_row = np.array(histogram)
  sampled_row = np.array([0] * len(histogram))
  total = sum(copied_row)
  for _ in range(no_samples):
    index = sample(copied_row, total)
    copied_row[index] -= 1
    assert copied_row[index] >= 0
    total -= 1
    assert total >= 0
    sampled_row[index] += 1
  return sampled_row



def compare_models(models):
  matrix = get_matrix_for_models(models)
  max_k = 10000
  # max_k should be less or equal to no of samples (# of rows in matrix)
  if max_k > matrix.shape[0]:
    max_k = matrix.shape[0]
  km, partitioning = kmeans(matrix, min_k=1, max_k=max_k)

  print("File: partition")
  for index, partition in sorted(enumerate(partitioning), key=lambda x: (x[1], x[0])):
    print("{} {} ".format(index, partition))



def plot_matrix(matrix, prefix):
  clustering = defaultdict(list)
  for _ in range(1):
    # clustering['kmeans'].append(kmeans(matrix, min_k=1, max_k=1, n_jobs=4))
    clustering['gmm'].append(gmm(matrix, min_k=1, max_k=5, n_jobs=4))

  fig_all = plt.figure(figsize=(4, 3))
  ax_all = fig_all.add_subplot(111)
  for index, column in enumerate(matrix.T):
    fig = plt.figure(figsize=(4, 3))
    ax = fig.add_subplot(111)
    for km, partitioning in clustering['kmeans']:
      for centroid in km.cluster_centers_:
        centroid_dimension = centroid[index]
        plt.axvline(centroid_dimension, linewidth=0.1, linestyle='--', color='red')

    histogram_data_to_plot = []
    histogram_data_colors = []
    for gm, partitioning in clustering['gmm']:
      for centroid in gm.means_:
        centroid_dimension = centroid[index]
        ax.axvline(centroid_dimension, linewidth=0.1, linestyle='-.', color='green')
        ax_all.axvline(centroid_dimension, linewidth=0.1, linestyle='-.', color='green')
      samples, labels = gm.sample(len(column))
      histogram_data_to_plot.append(samples.T[index])
      histogram_data_colors.append(('red', "model"))

      sample2 = norm(*norm.fit(column)).rvs(size=len(column))
      histogram_data_to_plot.append(sample2)
      histogram_data_colors.append(('green', 'single norm fit'))

    histogram_data_to_plot.append(column)
    histogram_data_colors.append(('blue', 'data'))
    bins = list(np.linspace(np.min(histogram_data_to_plot), np.max(histogram_data_to_plot), int(len(column) / 10)))
    for d, (color, label) in zip(histogram_data_to_plot, histogram_data_colors):
      ax.hist(d, bins=bins, histtype='step', color=color, label=label)
      ax_all.hist(column, bins=bins, histtype='step', color=color)

    fig.savefig("{}-{}-graph.pdf".format(prefix, index))
    plt.close(fig)
  fig_all.savefig("{}-all-graph.pdf".format(prefix))
  plt.close(fig_all)



def sample_data(data, no_samples):
  for _ in range(no_samples):
    index = randint(0, len(data) - 1)
    yield data[index]



def resample_model(data, model):
  model.__init__()
  for file in model.files:
    sampled_edges = sample_file(data[file], 100)
    model.add(get_traversal_count(sampled_edges))
  return model


def load_cached_models_from_directory(directory):
  import os.path
  assert os.path.isdir(directory)

  cache_file = os.path.join(directory, 'models.cache')
  if os.path.isfile(cache_file):
    models = load_objects(cache_file)
    # for index, model in enumerate(models):
    #   model.plot("mmm-{}.pdf".format(index))
    # exit(0)
    return models
  else:
    return []



def prioritize_edges_once(edges, test_case_matrix, runs_matrix):
  """sorts edges by finding the most important edges that influences the
   distance between runs and test cases.
   :returns index into edges that are selected
  """
  assert len(edges) == test_case_matrix.shape[1] == runs_matrix.shape[1]
  # some large value as initial threshold
  threshold = len(edges) * 1000
  selected_edges = []
  for i, edge in enumerate(edges):
    with Stopwatch("Deleting the edges"):
      masked_test_case_matrix = np.delete(test_case_matrix, i, axis=1)
      masked_runs_matrix = np.delete(runs_matrix, i, axis=1)
    with Stopwatch("calculating the distances"):
      distances = cdist(masked_runs_matrix, masked_test_case_matrix)
    current_max_min = np.mean(np.min(distances, axis=1))
    # Did the minimum distance decrease? Good!
    if current_max_min <= threshold:
      if current_max_min < threshold:
        logging.warning("current max distance is smaller: {} {} prev: {} {}"
                        .format(i, current_max_min, selected_edges, threshold))
        selected_edges = []
      selected_edges.append(i)
      threshold = current_max_min
  return threshold, selected_edges

def prioritize_edges_all(edges, test_case_matrix, runs_matrix, level=None):
  priorities = []
  if level is None:
    level = len(edges)
  while len(edges) > 0 and level > 0:
    level -= 1
    distance, selected_edges = prioritize_edges_once(edges, test_case_matrix, runs_matrix)
    logging.warning("selected_edges: {} {}".format(selected_edges, distance))
    test_case_matrix = np.delete(test_case_matrix, selected_edges, axis=1)
    runs_matrix = np.delete(runs_matrix, selected_edges, axis=1)
    logging.warning("edges: {}".format(edges))
    logging.warning("edges: {}".format(edges.shape))
    priorities.append([edges[i] for i in selected_edges])
    edges = np.delete(edges, selected_edges, axis=0)
    logging.warning("remaining edges: {}".format(edges.shape))
    logging.warning("test_case_matrix shape: {}".format(test_case_matrix.shape))
    logging.warning("runs_matrix shape: {}".format(runs_matrix.shape))
  logging.warning("priorities {}".format(priorities))
  return priorities


def prioritize_edges(edges, runs_matrix, test_case_matrix):
  """sorts edges by finding the most important edges that influences the
    distance between runs and test cases.
    :returns index into edges that are selected
   """
  assert len(edges) == test_case_matrix.shape[1] == runs_matrix.shape[1]
  # some large value as initial threshold
  edge_distances = []
  for i, edge in enumerate(edges):
    with Stopwatch("Deleting the edges"):
      masked_runs_matrix = np.delete(runs_matrix, i, axis=1)
      masked_test_case_matrix = np.delete(test_case_matrix, i, axis=1)
      assert masked_test_case_matrix.shape[0] == test_case_matrix.shape[0]
      assert masked_test_case_matrix.shape[1] == test_case_matrix.shape[1] - 1
      assert masked_runs_matrix.shape[0] == runs_matrix.shape[0]
      assert masked_runs_matrix.shape[1] == runs_matrix.shape[1] - 1


    with Stopwatch("calculating the distances"):
      distances = cdist(masked_runs_matrix, masked_test_case_matrix,
                        metric='cityblock')
    # current_max_min = np.mean(np.min(distances, axis=1))
    # current_max_min = np.max(np.min(distances, axis=1))
    # we use 'single' as a clustering method
    # -> we should use the min of min
    # in 'single' clustering the run that is closest to a test case
    # determines cluster merging.
    current_max_min = np.min(np.min(distances, axis=1))
    for x, row in enumerate(distances):
      for y, value in enumerate(row):
        if value == current_max_min:


          d2 = cdist(runs_matrix[[x]], test_case_matrix,
                        metric='cityblock')
          logging.warning("Edge {} Run {} is closest to {} distance: {} all data min dist: {}"
                          .format(edge, x, y, current_max_min, np.min(d2)))

          # logging.warning("test_case_matrix[{}][{}] {}".format(y, i, test_case_matrix[y][i]))
          # logging.warning("runs_matrix[{}][{}] {}".format(x, i, runs_matrix[x][i]))
          # logging.warning("test_case_matrix[{}] {}".format(y, list(test_case_matrix[y])))
          # logging.warning("runs_matrix[{}] {}".format(x, list(runs_matrix[x])))

    edge_distances.append(current_max_min)
  return edge_distances


from collections import namedtuple
Cluster = namedtuple('Cluster', ['id', 'members', 'distance'])

# Plot the flags
# (1) select test cases
def parse_linkage(linkage, num_data_points):
  population_for_all_iterations = []
  # population contains the entries in the linkage that survived
  # the merging. In the beginning, all the entries are members
  # use a dictionary because we want easy and cheap deletes below
  population = {k: Cluster(id=k, members=[k], distance=0) for k in range(num_data_points)}
  for iteration, linkage_entry in enumerate(linkage):
    # Run through the linkage. The linkage is ordered
    # by merging of the test cases
    # The test cases that got merged first are the ones
    # that are close to each other.
    logging.warning("Row to forget: {}".format(linkage_entry))
    try:
      # merged already
      members1 = population[linkage_entry[0]].members
      del population[linkage_entry[0]]
    except KeyError:
      members1 = [linkage_entry[0]]
    try:
      # merged already
      members2 = population[linkage_entry[1]].members
      del population[linkage_entry[1]]
    except KeyError:
      members2 = [linkage_entry[1]]
    id = num_data_points + iteration
    logging.warning("linkage: {}".format(linkage_entry))
    population[id] = Cluster(id=id, members=members1 + members2, distance=linkage_entry[2])
    population_for_all_iterations.append(dict(population))
  return population_for_all_iterations


# def sample_machine(machine, percentage):
#   ''' Sample the data of a machine
#   :param machine: a machine
#   :param percentage: sampling percentage
#   :return: a new machine with the sampled data
#   '''
#   name = "{} (sampled {}%)".format(machine.name, percentage)
#   new_machine = MachineModel(name)
#
#
#   return new_machine
#
#
# def sample_model(model, percentage):
#   '''samples the data in the model'''
#   new_model = Model()
#   new_machines = [sample_machine(machine, percentage) for machine in model.machines]
#   new_model.add(new_machines)
#   return new_model
#


def main():
  import sys
  models = []
  try:
    percentage = float(sys.argv[1])
    remaining_opts = sys.argv[2:]
  except ValueError:
    percentage = 100
    remaining_opts = sys.argv[1:]
  logging.warning("percentage: {}".format(percentage))

  for option in remaining_opts:
    logging.warning("argv: {}".format(option))
    models.append(load_model(option, percentage))
  model = models[0]
  other_models = models[1:]

  # calculate the common set of edges
  # reduce edges once
  # then always use these edges
  # benefit: the model does not have to be recreated because all edges are covered
  # otherwise a new model might introduce a new edge.
  # This will trigger the recalculation of the model.
  # Recalculation is costly
  all_edges = get_reduced_edges_for_models(models)

  # manually remove some edges: mostly for testing some hypothesis
  # edges_to_remove = np.array([[168, 0], [71, 1], [164, 2]])
  edges_to_remove = np.array([])
  edges_to_remove_flags = [False] * len(all_edges)
  for e in edges_to_remove:
    edges_to_remove_flags |= (all_edges == e).all(axis=1)
  edges_to_remove_flags = [not e for e in edges_to_remove_flags]
  all_edges = all_edges[edges_to_remove_flags,:]

  other_machines = []
  for a_model in other_models:
    other_machines.extend(a_model.machines)

  # all_edges is reduced already
  logging.warning("Getting matrices...")
  normalized = True
  _, model_matrix = get_matrix_for_models(model.machines, normalized=normalized,
                                          edges=all_edges)
  _, other_matrix = get_matrix_for_models(other_machines, normalized=normalized,
                                          edges=all_edges)

  # Reduce the rows for the test cases
  # if 2 test cases are the same,
  # we can remove them because we only check whether there is any test case in the cluster
  # not how many of them are there.
  to_remove_machines_idx, model_matrix = remove_same_rows(model_matrix)
  kept_machines = np.array(model.machines)[to_remove_machines_idx==False]
  logging.info("Removed {} test cases because their are exactly the same. "
               "(left {} test cases)".format(sum(to_remove_machines_idx), len(kept_machines)))

  # Do not reduce rows for the executions
  # Because later on we want to calculate a coverage
  # for the coverage to be accurate, we need all executions.
  # to_remove_other_machines_idx, other_matrix = remove_same_rows(other_matrix)
  # kept_other_machines = np.array(other_machines)[to_remove_other_machines_idx==False]
  kept_other_machines = other_machines

  if not normalized:
    # normalize globally
    model_matrix = (model_matrix.T / model_matrix.sum(axis=1)).T
    other_matrix = (other_matrix.T / other_matrix.sum(axis=1)).T
    # now normalize column wise
    # otherwise the graph is totally blue.
    # maybe we should do that after the dendogramm?
    # logging.warning("max in matrix: {}".format(model_matrix.max(axis=0)))
    # logging.warning("max in other_matrix: {}".format(other_matrix.max(axis=0)))
    # m = model_matrix.max(axis=0)
    # m[m == 0] = 1
    # model_matrix /= m
    # m = other_matrix.max(axis=0)
    # m[m == 0] = 1
    # other_matrix /= m
  complete_matrix = np.concatenate((model_matrix, other_matrix))
  logging.warning("In total we have {} executions with {} edges"
                  .format(*complete_matrix.shape))
  old_recursion_limit = sys.getrecursionlimit()
  sys.setrecursionlimit(2*old_recursion_limit)
  logging.warning("Set recursions limit to {}. Old limit was {}."
                  .format(sys.getrecursionlimit(), old_recursion_limit))

  fig = plt.figure(figsize=(80, 80))
  ax_top =    fig.add_axes([0.15, 0.85, 1-0.15, 0.15])
  ax_left =   fig.add_axes([0.0, 0, 0.1, 0.8])
  ax_middle = fig.add_axes([0.11, 0, 0.03, 0.8])
  ax_right =  fig.add_axes([0.15, 0, 1-0.15, 0.8])
  ax_flags =  fig.add_axes([0, 0, 1, 0.8])
  # ax_flags_y = fig.add_axes([0.15, 0, 1-0.15, 0.8])
  ax_flags_y =    fig.add_axes([0.15, 0.0, 1-0.15, 1])

  # plot top "x" dendrogram
  distance_metric = 'euclidean'
  distance_metric = 'cityblock' # need euclidean to use ward
  with Stopwatch("Calculating linkage for x-axis"):
    x_linkage = hierarchy.linkage(complete_matrix.T, method='ward')
  Z_x = hierarchy.dendrogram(x_linkage, ax=ax_top)
  ax_top.set_yticks([])
  sorted_edges = all_edges[Z_x['leaves'], :]
  ax_top.set_xticklabels(sorted_edges)

  # plot left "y" dendrogram
  # Y_linkage = hierarchy.linkage(complete_matrix, method='ward', metric=distance_metric)
  with Stopwatch("Calculating linkage for y-axis"):
    Y_linkage = hierarchy.linkage(complete_matrix, method='single',
                                metric=distance_metric)
  # Y_linkage = hierarchy.linkage(complete_matrix, method='centroid')
  # Y_linkage = hierarchy.linkage(complete_matrix, method='ward')
  # Y_linkage = hierarchy.linkage(complete_matrix, method='single', metric='cityblock')
  Z_y = hierarchy.dendrogram(Y_linkage, orientation='left', ax=ax_left)
  all_kept_machines = np.concatenate((kept_machines, kept_other_machines))
  all_kept_machines_sorted = all_kept_machines[Z_y['leaves']]
  names = list(map(lambda x: x.name, all_kept_machines_sorted))
  labels = ["{} {}".format(index, name) for index, name in enumerate(names)]
  test_case_counter = 0
  runs_counter = 0
  labels = []
  for index, name in enumerate(names):
    if "coreutils" in name:
      c = test_case_counter
      test_case_counter += 1
    else:
      c = runs_counter
      runs_counter += 1
    labels.append("{} {} {}".format(index, c, name))

  ax_left.set_yticklabels(labels)
  # UTF-8
  # plot vertical line at cutoffs
  # for l in [61.12428422903139, 6.442331649400538]:
  #   ax_left.axvline(x=l, c='k')

  # if not normalized:
  #   # normalize now
  #   m = model_matrix.max(axis=0)
  #   m[m == 0] = 1
  #   model_matrix /= m
  #   m = other_matrix.max(axis=0)
  #   m[m == 0] = 1
  #   other_matrix /= m
  #   complete_matrix = np.concatenate((model_matrix, other_matrix))

  # Plot the heatmap
  # sort the matrix according to the linkage
  idx1 = Z_y['leaves']
  idx2 = Z_x['leaves']
  sorted_matrix = complete_matrix[idx1, :]
  sorted_matrix = sorted_matrix[:, idx2]
  ax_right.matshow(sorted_matrix, aspect='auto', origin='lower', cmap='plasma')
  ax_right.set_xticks([])
  ax_right.set_yticks([])


  def calculate_coverage():
    values = []
    names_unsorted = list(map(lambda x: x.name, all_kept_machines))
    no_non_test_cases = sum("coreutils" not in name for name in names)
    # coverage is smaller than 1 to trigger print in first iteration
    prev_coverage = 0
    values.append((0, 0))
    area = 0
    population_for_all_iterations = parse_linkage(Y_linkage, complete_matrix.shape[0])
    logging.warning("Population in all : {}".format(len(population_for_all_iterations)))
    for popid, population in enumerate(population_for_all_iterations):
      # find clusters that do not have a test case
      # each population contains all the clusters at the iteration
      not_covered_runs = 0
      distance = 0
      for cluster in population.values():
        test_cases_in_cluster_flag = ["coreutils" in names_unsorted[member] for member in cluster.members]
        if not any(test_cases_in_cluster_flag):
          # no test case in whole cluster
          not_covered_runs += len(cluster.members)
        # logging.warning("no test: {}".format(sum(test_cases_in_cluster_flag)))
        # Use the maximum distance of all clusters. This is the current distance
        if distance < cluster.distance:
          distance = cluster.distance
      if no_non_test_cases == 0:
        logging.info("All executions are test cases.")
        coverage = 1
      else:
        coverage = (no_non_test_cases - not_covered_runs) / no_non_test_cases
      if coverage > prev_coverage:
        prev_distance = values[-1][0]
        area += (distance - prev_distance) * (1 - prev_coverage)
        values.append((distance, prev_coverage))
        values.append((distance, coverage))
        prev_coverage = coverage
    return area, values
  coverage, curve = calculate_coverage()
  logging.warning("coverage: {}".format(coverage))
  logging.warning("curve: {}".format(curve))
  save_object("curve", (complete_matrix.shape, curve, model_matrix, other_matrix))

  for iu, name in enumerate(names):
    logging.warning("names iu: {} name: {}".format(iu, name))

  #######
  # Highlight test case and runs
  #######

  def highlight(flags, ax, alpha=1):
    ax.clear()
    ax.matshow(flags, aspect='auto', origin='lower', cmap='Greys', alpha=alpha)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.patch.set_alpha(0)

  flags = np.array([[1] if "coreutils" in name else [0] for name in names])
  highlight(flags, ax_flags, 0.2)
  highlight(flags, ax_middle)


  #########
  # Calculate edges distances
  #########

  sorted_matrix_x_only = complete_matrix[idx1, :] # have to sort by X because we use sorted X indices below
  # ls
  # runs_sub_matrix = sorted_matrix_x_only[1029:1184+1,:]
  # wc
  # runs_sub_matrix = sorted_matrix_x_only[148:185+1,:]
  # logging.warning("names sub: {}".format(names[148:185+1]))
  # sort
  # runs_sub_matrix = sorted_matrix_x_only[[127, 128],:]
  # runs_sub_matrix = sorted_matrix_x_only[[105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 123, 122, 127, 128],:]
  # runs_sub_matrix = sorted_matrix_x_only[list(range(255, 269)),:]
  # else
  runs_sub_matrix = sorted_matrix_x_only[["coreutils" not in name for name in names]]

  ###
  # sampling
  ###
  # ls 1%
  runs_sub_matrix = sorted_matrix_x_only[-38:,:]
  logging.warning("run matrix CCC: {}".format(runs_sub_matrix))
  logging.warning("run matrix CCC: {}".format(runs_sub_matrix.shape))



  test_case_matrix_x_only = sorted_matrix_x_only[["coreutils" in name for name in names]]
  logging.warning("Before clac distances FFF: {}".format(runs_sub_matrix.shape))
  logging.warning("Before clac distances FFF: {}".format(test_case_matrix_x_only.shape))
  distances = prioritize_edges(all_edges, runs_sub_matrix, test_case_matrix_x_only)

  logging.warning("Minimum at index: {}".format(distances.index(np.min(distances))))
  logging.warning("distances: {}".format(distances))
  lower = np.min(distances)
  upper = np.max(distances)
  logging.warning("lower: {} upper: {}".format(lower, upper))
  # one row with many flags (because it is plotted on y-axis)
  if upper == lower:
    distances = [0] * len(distances)
  else:
    distances = [1 - ((distance - lower) / (upper - lower)) for distance in distances]
  for distance, edge in sorted(zip(distances, all_edges), key=lambda x: x[0]):
    logging.warning("distance, edge {} {}".format(distance, edge))

  edge_flags = np.array(distances)[Z_x['leaves']]
  edge_flags = np.array([edge_flags])
  edge_flags /= 2
  edge_flags += 0.5
  # edge_flags = np.array([[0 if e[0] == 706 else 1 for e in sorted_edges]])
  highlight(edge_flags, ax_flags_y, 0.4)

  plt.savefig("a.png")
  plt.close()  # distances = prioritize_edges_all(all_edges, test_case_matrix, runs_matrix)
  exit(0)



  ############
  # old stuff
  ############
  def select_test_cases(no_test_case_to_keep):
    n = complete_matrix.shape[0]
    # select test case
    selected = set()
    population = population_for_all_iterations[-1 * no_test_case_to_keep]
    for cluster in population.values():
      selected.add(cluster.members[0])
    logging.warning("selected: {}".format(selected))
    test_case_flags = [[0] if t in selected else [1] for t in range(n)]
    return test_case_flags

  no_of_observations = complete_matrix.shape[0]
  for no_of_test_cases in reversed(range(0, no_of_observations, 10)):
    test_case_flags = select_test_cases(no_of_test_cases)
    # plot flags
    flags = np.array([[1]] * model_matrix.shape[0] + [[0]] * other_matrix.shape[0])
    flags = np.array(test_case_flags)
    # sort the flags
    idx1 = Z_y['leaves']
    flags = flags[idx1]
    highlight(flags)

    plt.savefig("a-{}.png".format(no_of_test_cases))
    # plt.savefig("a-{}.pdf".format(no_of_test_cases))


  plt.close()

  exit(0)




  def find_outliers(models, prefix=''):
    edges, matrix_non_normalized, matrix_normalized = get_reduced_matrices_for_models(models)
    # Do a KMeans
    from multiprocessing import Pool
    from functools import partial
    with Pool(processes=1) as pool:
      to_check = range(len(models))
      outliers_map = pool.map(partial(is_outlier_pool, matrix_normalized=matrix_normalized,
                                      matrix_non_normalized=matrix_non_normalized, edges=edges),
                                      to_check)
    outliers = []
    for index, this_is_an_outlier, est, machines_sharing_cluster, z_scores in outliers_map:
      if this_is_an_outlier:
        # machine_data = np.empty((len(machines_sharing_cluster)))
        machine_data = np.matrix([matrix_normalized[machine] for machine in machines_sharing_cluster])
        outliers.append((models[index], est, index, matrix_normalized[index], machine_data, edges, z_scores))
    outliers.sort(key=lambda x: x[1])
    return outliers


  if False:
    try:
      model = load_object("model.all")
    except:
      model = Model()
      model.add(all_machines)
      model.gm_model
      save_object("model.all", model)

  for a_model in other_models:
    #for a_model in models:
    for machine in a_model.machines:
      outliers = model.find_outliers([machine], edges=all_edges)
      if outliers:
        logging.warning("{} detected as outlier".format(machine))
      else:
        logging.warning("{} is not an outlier".format(machine))

  exit(0)


  # for model in models:
  #   resample_model(raw_data, model)
  # save_objects("models-resampled.cache", models)
  # write_daikon_file('daikon.csv', column_names, matrix)


  # read C.coverage and UTF8.coverage
  # check whether they are outliers.
  # c_loader = LazyDataLoader("C.coverage")
  # for to_check_dir in ["C", "bb-utf8", "UTF8"]:
  for to_check_dir in ["C", "UTF8"]:
    # for to_check_dir in ["UTF8"]:
    loader = LazyDataLoader(to_check_dir)
    machine = create_machine(loader)
    machine.name = to_check_dir
    logging.warning("Checking {}".format(machine))
    outliers = model.find_outliers([machine])
    if outliers:
      logging.warning("{} detected as outlier".format(machine))
    else:
      logging.warning("{} is not an outlier".format(machine))

  exit(0)




  def fetch_machines_from_directory(no_of_machines, no_of_executions, directory):
    models = load_cached_models_from_directory(directory)
    loader = LazyDataLoader(directory)
    if len(models) > no_of_machines:
      models = models[:no_of_machines]
    elif len(models) < no_of_machines:
      for model_no in range(no_of_machines - len(models)):
        logging.info("Creating model {}".format(model_no))
        # model = create_model(raw_data, files_to_sample, no_of_executions, [], 0)
        logging.info("Creating complete model {}".format(model_no))
        model = create_machine(loader)
        model.name = model_no
        models.append(model)
      save_objects(os.path.join(directory, "models.cache"), models)
    return models, loader



  no_of_executions = 500
  machines, raw_data = fetch_machines_from_directory(100, no_of_executions, sys.argv[1])
  files_to_sample = list(raw_data.keys())

  to_check, to_check_data = fetch_machines_from_directory(100, no_of_executions, sys.argv[2])
  model = Model()
  model.add(machines)
  for machine in to_check:
    outliers = model.find_outliers([machine])
    if outliers:
      model.add([machine])
      outliers = model.find_outliers([machine])
      if outliers:
        logging.warning("Still an outlier even in the new corpus")
      else:
        logging.warning("Not an outlier anymore in the new corpus")
    else:
      logging.warning("Not an outlier")
  exit(0)
  machines.extend(to_check)

  # from itertools import chain
  # for percentage in chain(range(6), range(10, 101, 5)):
  #   for percentage in chain(range(30, 41, 5)):
  for percentage in [4,5,10,30,40]:
    no_of_bad_samples = int(round(no_of_executions * percentage / 100))
    no_of_good_samples = no_of_executions - no_of_bad_samples
    model = create_machine(raw_data, files_to_sample, no_of_good_samples, ["96.coverage"], no_of_bad_samples)
    model.name = "Bad model ({}%)".format(percentage)
    machines[6] = model

    outliers = find_outliers(machines, percentage)
    print("Percentage: {} Outliers: {}".format(percentage, len(outliers)))
    for _, est, index, outlier_data, machine_data, edges, z_scores in outliers:
      centroid = np.mean(machine_data, 0)
      distance = np.linalg.norm(centroid - outlier_data)
      print("Outlier: {} probability: {} support: {} distance: {}"
            .format(index, est, len(machine_data), distance))
      print("\t{}".format(edges))
      print("z_scor.\t{}".format(np.round(z_scores, decimals=3)))
      print("my tot\t{}".format(machines[index].get_data_for_edges(edges, normalized=False)))
      print("myself\t{}".format(np.round(outlier_data, decimals=3)))
      print("mean\t{}".format(np.round(centroid, decimals=3)))
      for d in np.round(machine_data, decimals=3):
        print("\t{}".format(d))
      for d in machine_data:
        distance = np.linalg.norm(d - outlier_data)
        print("\tdistance: {}".format(distance))
      for d in machine_data:
        distance = np.linalg.norm(d - centroid)
        print("\tdist. center: {}".format(distance))
      machines[index].plot("outlier-{}-{}.pdf".format(percentage, index))
      # from collections import Counter
      # logging.warning("model {} has files: {}".format(index, Counter(models[index].files)))

    sys.stdout.flush()
    # daikon
    # daikon_files = convert_daikon_csv('daikon.csv')
    # run_daikon(daikon_files)
    # run_daikon_for_all_nodes(column_names, matrix)


if __name__ == '__main__':
  main()