import logging
logging.getLogger().setLevel("INFO")

from model import *
#
# def edge_coverage_difference(models1, models2):
#   """Find the difference in basic blocks
#
#   :returns a tuple (only1, only2) of basic blocks that are only in
#   models1 (only1) and models2 (only2)"""
#   edges1 = set()
#   for m in models1:
#     for machine in m.machines:
#       edges1.update(*machine.edges())
#   edges2 = set()
#   for m in models2:
#     for machine in m.machines:
#       edges2.update(*machine.edges())
#   delta1 = edges1 - edges2
#   delta2 = edges2 - edges1
#   return delta1, delta2

def get_all_edges(models):
  for model in models:
    for machine in model.machines:
      yield from machine.edges()

from collections import namedtuple
BasicBlock = namedtuple('BasicBlock', ['id', 'size', 'conditional', 'myself', 'successors', 'default'])

def parse_instrumentation_log(path):
  blocks = []
  with open(path, 'r') as f:
    for line in f:
      line = line.strip()
      # >>> Basic block: 1....
      pattern = '>>> Basic block: '
      if not line[:len(pattern)] == pattern:
        continue
      # >>> Basic block: 19 src/system.h:646:5 size: 4 conditional: 0 myself: 0x6ffd450 successors: 0x6ffd4a0
      # >>> Basic block: 294 src/df.c:1032:7 size: 9 conditional: 1 myself: 0x736d810 successors: 0x73a02a0 ...
      #   switch: 0x736d860:0:0 ... default: 0x73a02a0


      fields = line.split(' ')
      id = int(fields[3])
      size = int(fields[6])
      conditional = int(fields[8]) == 1
      myself = int(fields[10], 16)
      logging.info("succ: {}".format(fields[12]))

      successors = []
      for index, s in enumerate(fields[12].split(',')):
        if len(s) > 0:
          successors.append((int(s, 16), index))

      if fields[13] == 'switch:':
        logging.warning("found a switch")
        successors = []
        for s in fields[14].split(','):
          if len(s) > 0:
            successor, value,index = s.split(':')
            successors.append((int(successor, 16), value))
      default = int(fields[-1], 16)
      if default == 0:
        default = None
      logging.info("myself: {}".format(myself))
      logging.info("successors: {}".format(successors))
      logging.info("def: {}".format(default))

      blocks.append(BasicBlock(id, size, conditional, myself, successors, default))
  return blocks

def main(argv):
  # # remove all non covered bbs
  # _, not_covered_bb = statement_coverage_difference([model], other_models)
  # logging.warning("Uncovered bb: {}".format(not_covered_bb))
  # filter_map = list(map(lambda x: x[0] not in not_covered_bb, all_edges))
  # all_edges = all_edges[filter_map]
  models = []
  # for option in argv[2:]:
  #   models.append(load_model(option))
  # assert len(models) == 3

  blocks = parse_instrumentation_log(argv[1])
  blocks_by_myself = {block.myself: block for block in blocks}
  blocks_by_id = {block.id: block for block in blocks}

  def get_executed_bbs(file):
    for edge in read(file):
      source, target = edge
      block = blocks_by_id[source]
      yield block
      continue
      # emit all following blocks that end with unconditional jumps
      def get_next_block(block, edge):
        if not block.successors:
          return None
        if len(block.successors) == 1:
          return block.successors[0][0]
        if edge is None:
          return None
        source, target = edge
        for dst_block_myself, value in block.successors:
          if value == target:
            return dst_block_myself
        if block.default is None:
          raise ValueError("target {} not found {}".format(target, block))
        return block.default

      while block is not None:
        block_myself = get_next_block(block, edge)
        edge = None
        if block_myself is None:
          break
        block = blocks_by_myself[block_myself]
        if len(block.successors) < 2:
          yield block
        else:
          break

  covered = []
  for option in argv[2:]:
    files = LazyDataLoader(option).files
    covered_bb = []
    covered_bb_ids = set()
    for file in files:
      for bb in get_executed_bbs(file):
        if bb.id not in covered_bb_ids:
          covered_bb_ids.add(bb.id)
          covered_bb.append(bb)
    print("Covered {} basic blocks. Total basic blocks: {} Coverage: {}"
          .format(len(covered_bb), len(blocks), len(covered_bb) / len(blocks)))
    instructions = sum(bb.size for bb in covered_bb)
    total_instructions = sum(bb.size for bb in blocks)
    print("Covered {} instructions. Total instrucitons: {} Coverage: {}".format(instructions, total_instructions, instructions / total_instructions))
    all_block_ids = set(bb.id for bb in blocks)
    covered_block_ids = set(bb.id for bb in covered_bb)
    covered.append(covered_block_ids)

  print("Residual testing: {}".format(covered[1] & (all_block_ids - covered[0])))

  from sys import exit
  exit(0)
  sizes = {block.id: block.size for block in blocks}


  unique_edges_test_runs = set(get_all_edges([models[0]]))
  unique_edges_real_runs = set(get_all_edges(models[1:]))
  print("Test runs covered {} edges.".format(len(unique_edges_test_runs)))
  print("Real runs covered {} edges.".format(len(unique_edges_real_runs)))

  covered_basic_blocks_test_runs = set(edge[0] for edge in unique_edges_test_runs)
  covered_basic_blocks_real_runs = set(edge[0] for edge in unique_edges_real_runs)
  total_basic_blocks = len(sizes.keys())
  print("Test runs covered {} basic blocks. Fraction {}"
                  .format(len(covered_basic_blocks_test_runs),
                          len(covered_basic_blocks_test_runs) / total_basic_blocks
                          ))
  print("Real runs covered {} basic blocks. Fraction {}"
                  .format(len(covered_basic_blocks_real_runs),
                          len(covered_basic_blocks_real_runs) / total_basic_blocks
                          ))
  print("Uncovered basic blocks executed in real: {}"
        .format(covered_basic_blocks_real_runs - covered_basic_blocks_test_runs))

  #
  # covered_instructions_test_runs = sum(sizes[bb] for bb in covered_basic_blocks_test_runs)
  # covered_instructions_real_runs = sum(sizes[bb] for bb in covered_basic_blocks_real_runs)
  # total_instructions = sum(sizes.values())
  # logging.warning("WARNING: These instructions do not include instructions that are reached via unconditional branches")
  # logging.warning("Test runs covered {} instructions. Coverage: {}"
  #                 .format(len(covered_instructions_test_runs),
  #                             covered_instructions_test_runs / total_instructions))
  # logging.warning("Real runs covered {} instructions. Coverage: {}"
  #                 .format(len(covered_instructions_real_runs),
  #                             covered_instructions_real_runs / total_instructions))
  #
  #



if __name__ == '__main__':
  import sys
  main(sys.argv)