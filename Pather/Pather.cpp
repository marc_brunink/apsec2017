#include "llvm/Pass.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Module.h"
#include <llvm/IR/Constants.h>
#include <llvm/IR/LLVMContext.h>
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/IRBuilder.h"

#include <cstdio>
#include <sstream>
#include <boost/iterator/iterator_concepts.hpp>
using std::stringstream;
#include "llvm/Support/CommandLine.h"
using namespace llvm;

static cl::opt<std::string> id_string_opt("patherid", cl::desc("Specify id string for Pather"), cl::value_desc("id string"));

namespace {
  void log_BB(BasicBlock& BB, unsigned bbID, bool conditional, BasicBlock* defaultBB=NULL) {
DebugLoc dbg = NULL;
      for (auto ritr = BB.rbegin(); !dbg && ritr != BB.rend(); ritr++) {
        dbg = ritr->getDebugLoc();
      }
     // DebugLoc dbg = BB.getFirstInsertionPt()->getDebugLoc();
      errs() << ">>> Basic block: " << bbID << " ";
      if (dbg) {
        dbg.print(errs());
      }
      else {
        errs() << "unknown:0:0";
      }
      errs() << " size: " << BB.size();
      errs() << " conditional: " << conditional;
      errs() << " myself: " << &BB;
      errs() << " successors: ";
	auto terminator = BB.getTerminator();     
	if (terminator) 
for (unsigned i=0; i<terminator->getNumSuccessors(); i++) {
	errs() << terminator->getSuccessor(i) << ",";
	}

      if (auto switch_ = dyn_cast<SwitchInst>(terminator)) {
      errs() << " switch: ";
          for (auto case_itr = switch_->case_begin(); case_itr != switch_->case_end(); ++case_itr) {
            int64_t va = case_itr.getCaseValue()->getValue().getSExtValue();
	errs() << case_itr.getCaseSuccessor() << ":" << va << ":" << case_itr.getCaseIndex();
	errs() << ",";
}}
      errs() << " default: " << defaultBB;
      errs() << "\n";

}


  struct Pather : public BasicBlockPass {
    static char ID;
    Function *func_fprintf;
    Constant *id_string;
    unsigned bbID;

    Pather() : BasicBlockPass(ID), func_fprintf(nullptr), id_string(nullptr), bbID(0) {}

    bool doInitialization(Function& function) {
      //       auto func_type = FunctionType::get(TypeBuilder<int, false>::get(function.getContext()), {TypeBuilder<FILE*, false>::get(function.getContext()), TypeBuilder<char*, false>::get(function.getContext())}, true);

      auto func_type = TypeBuilder<void(int, int, const char*), false>::get(function.getContext());

      func_fprintf = cast<Function>(function.getParent()->getOrInsertFunction("print_BB",
                                                                              func_type
      ));
      assert (func_fprintf);

      /*
      auto format_string_array = ConstantDataArray::getString(function.getContext(), id_string_opt);
      auto& mod = *function.getParent();

      id_string = mod.getGlobalVariable("__pather_id_string");
      if (!id_string) {
        id_string = new GlobalVariable(mod,
                                                 format_string_array->getType(), true,
                                                   GlobalValue::ExternalLinkage,
                                                 format_string_array, "__pather_id_string");
      }
      assert(id_string);
      */
      return false;
    }

    bool runOnBasicBlock(BasicBlock& BB) override {
      if (BB.getParent() == func_fprintf)
        return false;
      ++bbID;

      auto terminator = BB.getTerminator();
      if (!terminator) {
        errs() << "Basic block is not well formed: " << BB << "\n";
        return false;
      }

      if (terminator->getNumSuccessors() < 1) {
	// e.g. return statements
  	log_BB(BB, bbID, false);
        return false;
      }

      Value* value = nullptr;
      Value* add_value = nullptr;
	BasicBlock* defaultBB = NULL;
      if (auto branch = dyn_cast<BranchInst>(terminator)) {
        if (branch->isUnconditional()) {
  	  log_BB(BB, bbID, false);
          return false;
        }
        Value* condition = branch->getCondition();
        value = CastInst::CreateIntegerCast(condition,IntegerType::getInt32Ty(BB.getContext()), false, "br-cond-cast", terminator);
      }
      else {
        if (auto switch_ = dyn_cast<SwitchInst>(terminator)) {
          Value* condition = switch_->getCondition();
          value = CastInst::CreateIntegerCast(condition,IntegerType::getInt32Ty(BB.getContext()), true, "br-cond-cast", terminator);
	defaultBB = switch_->getDefaultDest();
          std::stringstream ss;
          bool first = true;
          for (auto case_itr = switch_->case_begin(); case_itr != switch_->case_end(); ++case_itr) {
            int64_t va = case_itr.getCaseValue()->getValue().getSExtValue();
            errs() << "case " << bbID << " value: " << va << "\n";
            if (!first)
              ss << ",";
            first = false;
            ss << va;
          }
          std::string string(ss.str());
          IRBuilder<> builder(terminator);
          add_value = builder.CreateGlobalStringPtr(ss.str());
        }
        else {
          errs() << "Unhandled terminator: " << *terminator << "\n";
        }
      }
      assert(value);
        // So we have 2 sucecssors exactly
      auto id = ConstantInt::get(IntegerType::getInt32Ty(BB.getContext()), bbID);
      CallInst::Create (func_fprintf, {id, value, add_value ? add_value : ConstantPointerNull::get(PointerType::get(IntegerType::getInt8Ty(BB.getContext()), 0))}, "", terminator);

      log_BB(BB, bbID, true, defaultBB);
      return true;
    }
  };
}

char Pather::ID = 0;
static RegisterPass<Pather> X("pather", "log all basic blocks", false, false);
