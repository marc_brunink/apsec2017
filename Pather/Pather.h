#ifndef Pather_H
#define Pather_H

#include <QtCore/QObject>

class Pather : public QObject
{
Q_OBJECT
public:
    Pather();
    virtual ~Pather();
private slots:
    void output();
};

#endif // Pather_H
