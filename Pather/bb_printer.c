#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <string.h>


//extern const char __pather_id_string[];
const char __pather_id_string[] = "NAME";


void print_BB(int id, int direction, const char* string) {

  static char* directory = NULL;
  static __thread FILE* file = NULL;
  static __thread pid_t pid = 0;
  if (!directory) {
    static char template[1024];
    snprintf(template, 240, "/home/marc/.bb/%s-XXXXXX", __pather_id_string);


    // We do not need to protect this with a lock
    // The very first basic block will execute this.
    // We wont have threads at this point in time
    directory = mkdtemp(template);
    if (!directory) {
       perror("Creating the temp directory failed!");
       return;
    }
  }

  assert(directory);
  if (pid != getpid()) {
    //We forked (or first executed bb)
    pid = getpid();
    file = NULL;
  }
  if (!file) {
    // Let us assume tids and pids are not recycled
    // in a single run
    pid_t tid = syscall(SYS_gettid);
    assert(tid < 100000);
    const char* format = "%s/%i-XXXXXX.coverage";
    char* path = malloc(sizeof(char) * (strlen(format) - 2 + strlen(directory) - 2 + 5));
    sprintf(path, format, directory, tid);
    int fd = mkstemps(path, strlen(".coverage"));
    file = fdopen(fd, "w");
    free(path);
  }
  if (!string) string = "";
  fprintf(file, "%i %i %s\n", id, direction, string);
  fflush(file);
}
